/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;
import java.util.Scanner;
/**
 *
 * @author hp
 */
public class TestFactura {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Factura factura = new Factura();
        Scanner sc = new Scanner(System.in);
        int opcion = 0, numFactura = 0;
        String rfc = "", nombreCliente = "", domFiscal = "", descripcion = "", fechaVenta = "";
        float totalVenta = 0.0f;
        
        do{
            System.out.println("1. Capturar factura");
            System.out.println("2. Modificar valores");
            System.out.println("3. mostrar factura");
            System.out.println("4. salir");
            
            System.out.println("Dame la opcion");
            opcion = sc.nextInt();
            switch(opcion){
                case 1:
                    System.out.println("Num Factura :");
                    factura.setNumFactura(sc.nextInt());
                    System.out.println("RFC :");
                    factura.setRfc(sc.next());
                    sc.nextLine();
                    System.out.println("Nombre del cliente :");
                    factura.setNombreCliente(sc.nextLine());
                    System.out.println("Domicilio fiscal :");
                    factura.setDomFiscal(sc.nextLine());
                    System.out.println("Descripcion ");
                    factura.setDescripcion(sc.nextLine());
                    System.out.println("Fecha de la venta :");
                    factura.setFechaVenta(sc.nextLine());
                    System.out.println("Total de la venta");
                    factura.setTotalVenta(sc.nextFloat());
                    break;
                
                case 2:
                    do{
                        System.out.println("1. Num Factura");
                        System.out.println("2. RFC");
                        System.out.println("3. Nombre del cliente");
                        System.out.println("4. Domicilio Fiscal");
                        System.out.println("5. Descripcion");
                        System.out.println("6. Fecha de la venta");
                        System.out.println("7. Total de la venta");
                        System.out.println("8. Salir");
                        
                        System.out.println("Opcion :");
                        opcion = sc.nextInt();
                        switch(opcion){
                            case 1:
                                System.out.println("Num Factura :");
                                factura.setNumFactura(sc.nextInt());
                                sc.nextLine();
                                break;
                            case 2:
                                System.out.println("RFC :");
                                factura.setRfc(sc.next());
                                sc.nextLine();
                                break;
                            case 3:
                                System.out.println("Nombre del cliente :");
                                factura.setNombreCliente(sc.next());
                                sc.nextLine();
                                break;
                            case 4:
                                System.out.println("Domicilio fiscal :");
                                factura.setDomFiscal(sc.next());
                                sc.nextLine();
                                break;
                            case 5:
                                System.out.println("Descripcion ");
                                factura.setDescripcion(sc.next());
                                sc.nextLine();
                                break;
                            case 6:
                                System.out.println("Fecha de la venta :");
                                factura.setFechaVenta(sc.next());
                                sc.nextLine();
                                break;
                            case 7:
                                System.out.println("Total de la venta");
                                factura.setTotalVenta(sc.nextFloat());
                                sc.nextLine();
                                break;
                            case 8:
                                System.out.println("Regresar al menu principal");
                                break;
                            default:
                                System.out.println("No es una opcion valida");
                                break;
                        }
                    }while(opcion!=8);
                    break;   
                case 3:
                    factura.imprimirFactura();
                    System.out.println("Oprima una tecla para continuar");
                    sc.next();
                    break;
                case 4:
                    System.out.println("Gracias.......");
                    break;
                default :
                    System.out.println("Opcion invalida");
                    break;
            }
            
        }while(opcion!=4);
    }
    
}
