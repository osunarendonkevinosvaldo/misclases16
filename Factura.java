/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author hp
 */
public class Factura {
    private int numFactura;
    private String rfc;
    private String nombreCliente;
    private String domFiscal;
    private String descripcion;
    private String fechaVenta;
    private float totalVenta;
    
    public Factura(){
        //constructor por omision
        this.numFactura = 0;
        this.rfc = "";
        this.nombreCliente = "";
        this.domFiscal = "";
        this.descripcion = "";
        this.fechaVenta = "";
        this.totalVenta = 0;
        
    }

    public Factura(int numFactura, String rfc, String nombreCliente, String domFiscal, String descripcion, String fechaVenta, float totalVenta) {
        //constructor por omision
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombreCliente = nombreCliente;
        this.domFiscal = domFiscal;
        this.descripcion = descripcion;
        this.fechaVenta = fechaVenta;
        this.totalVenta = totalVenta;
    }
    public Factura(Factura otro){
        //constructor por copia
        this.numFactura = otro.numFactura;
        this.rfc = otro.rfc;
        this.nombreCliente = otro.nombreCliente;
        this.domFiscal = otro.domFiscal;
        this.descripcion = otro.descripcion;
        this.fechaVenta = otro.fechaVenta;
        this.totalVenta = otro.totalVenta;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomFiscal() {
        return domFiscal;
    }

    public void setDomFiscal(String domFiscal) {
        this.domFiscal = domFiscal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String Descripcion) {
        this.descripcion = Descripcion;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
    
    public float calcularImpuesto(){
        float impuesto = 0.0f;
        impuesto = this.totalVenta * .16f;
        return impuesto;
    }
    public float calcularTotalPagar(){
        float total = 0.0f;
        total = this.totalVenta + this.calcularImpuesto();
        return total;
    }
    public void imprimirFactura(){
        System.out.println("Numero de factura :" + this.numFactura);
        System.out.println("RFC :" + this.rfc);
        System.out.println("Nombre del cliente :" + this.nombreCliente);
        System.out.println("Domicilio fiscal :" + this.domFiscal);
        System.out.println("Descripcion :" + this.descripcion);
        System.out.println("Fecha de venta :" + this.fechaVenta);
        System.out.println("Total de venta :" + this.totalVenta);
        System.out.println("Impuesto :" + this.calcularImpuesto());
        System.out.println("Total a pagar :" + this.calcularTotalPagar());
    }
            
}
//termina la clase
